/* jshint forin: false*/

var isObject = function ( obj ) {
  return typeof {} === typeof obj;
};

var extend = function ( base, ext ) {
  for ( var i in ext ) {
    if ( base[ i ] ) {
      throw "key " + i + " already exists in the module";
    }
    base[ i ] = ext[ i ];
  }
};

var mod = function ( key, obj ) {
  var fn, extension;

  fn = this.fn = this.fn || {};

  if ( !key && !obj ) {
    throw "Please, pass at least a module name";
  }

  if ( fn[ key ] && !obj ) {
    return fn[ key ];
  }

  if ( !fn[ key ] && !obj ) {
    return null;
  }

  extension = isObject( obj ) ? obj : obj.apply( {}, [ this ] );

  if ( !fn[ key ] && obj ) {
    fn[ key ] = extension;
  } else {
    extend( fn[ key ], extension );
  }

  return fn[ key ];
};

module.exports = {
  attach: function ( sitecore ) {
    if ( sitecore.module ) {
      return;
    }
    sitecore.module = mod.bind( sitecore );
  }
};