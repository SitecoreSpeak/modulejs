var moduleJS = require( "../src/index" ),
  should = require( "should" );

describe( "Given the module ", function () {
  it( "should be defined", function () {
    moduleJS.should.exist;
  } );
  describe( "When I am attaching the module to an object ", function () {
    var test = {};
    it( "it should have a module function", function () {
      moduleJS.attach( test );
      test.module.should.exist;
    } );
    describe( "When I want to get a non existing module ", function () {
      it( "it should return null", function () {
        var object = test.module( "test" );
        ( object === null ).should.be.true;
      } );
    } );
    describe( "When I want to create a new module ", function () {
      it( "it should return the module", function () {
        var object = test.module( "test", {
          test: function () {
            return true;
          }
        } );
      } );
      it( "we should be able to get the module", function () {
        var testModule = test.module( "test" );

        testModule.should.exist;
        testModule.test.should.exist;
        testModule.test().should.equal( true );
      } );
      it( "should be able to extend module with an object", function () {
        var testModule = test.module( "test", {
          test2: function () {
            return true;
          }
        } );
        testModule.test.should.exist;
        testModule.test2.should.exist;
      } );
      it( "should be able to extend module with a function", function () {
        var testFunctionModule = test.module( "test", function () {
          var test = 5;
          return {
            testFunction: test,
          };
        } );

        testFunctionModule.test.should.exist;
        testFunctionModule.test2.should.exist;
        testFunctionModule.testFunction.should.exist;
        testFunctionModule.testFunction.should.equal( 5 );
      } );
      it( "should throw if trying to extend already exisint function", function () {
        ( function () {
          test.module( "test", {
            test2: true
          } );
        } ).should.
        throw ();
      } );
    } );
    describe( "When passing invalid parameters", function () {
      it( "should throw", function () {
        ( function () {
          test.module();
        } ).should.
        throw ();
      } );
    } );
  } );
} );